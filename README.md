VIP180 All-in-One Deployment Tools
====

This application is used to deploy a standard BIP180 token smart contract.

# Install

	npm install

# Usage

	npm run deploy


+ endpoint: The HTTP provider for VeChainThor network
+ master private key: The signer to deploy the contract
+ initialHolder: The initial VIP180 token holders
+ name: The name of token
+ symbol: The symbol of token
+ decimals: The number of decimals the token uses 

# Screenshot

![deploy](images/deploy.jpg)


